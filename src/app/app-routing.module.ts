import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReadComponent } from './pages/read/read.component';
import { EditComponent } from './pages/edit/edit.component';

export enum Routing {
  add = 'add',
  edit = 'edit/:id',
}

const routes: Routes = [
  { path: Routing.add, component: EditComponent },
  { path: Routing.edit, component: EditComponent },
  {
    path: '',
    component: ReadComponent,
  },
  {
    path: '**',
    component: ReadComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
