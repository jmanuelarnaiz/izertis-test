// Angular Core
import { Component, OnDestroy, OnInit } from '@angular/core';

// Angular Forms
import { FormControl, FormGroup, Validators } from '@angular/forms';

// Routes
import { ActivatedRoute } from '@angular/router';

// Subscription
import { Subscription } from 'rxjs';

// Alerts
import Swal from 'sweetalert2';

// Models
import { Post } from '../../models/post.model';

// Services
import { DataService } from '../../../services/data/data.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss'],
})
export class EditComponent implements OnInit, OnDestroy {
  public isEditMode: boolean;
  public postForm: FormGroup;
  public submitted: boolean;

  private _subscriptions: Subscription[];
  private _postId: string;

  constructor(
    private _route: ActivatedRoute,
    private _dataService: DataService
  ) {
    this.isEditMode = false;
    this._subscriptions = [];
    this.submitted = false;
    this._postId = '';
    this.postForm = new FormGroup({
      body: new FormControl(null, Validators.required),
      title: new FormControl(null, Validators.required),
    });
  }

  ngOnInit(): void {
    this._subscriptions.push(
      this._route.params.subscribe((params) => {
        this._postId = params['id'];
        this.isEditMode = !!this._postId;
        if (this.isEditMode) {
          // Edit Mode
          this._retrievePost(this._postId);
        }
      })
    );
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((suscription) => suscription.unsubscribe());
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.postForm.controls;
  }

  async onPostSubmit() {
    this.submitted = true;
    if (this.postForm.valid) {
      const post: Post = {
        id: this._postId,
        body: this.postForm.get('body').value,
        title: this.postForm.get('title').value,
        userId: '1', //TODO: Hardcoded, should be user logged id
      };
      if (this.isEditMode) {
        // Edit mode
        try {
          await this._dataService.updatePost(post);
          Swal.fire(
            'Update',
            `Post ${this._postId} have been successfully updated`,
            'success'
          );
        } catch (error) {
          console.error('Error updating POST');
        }
      } else {
        // Create mode
        try {
          await this._dataService.createPost(post);
          Swal.fire('Create', `Post have been successfully created`, 'success');
        } catch (error) {
          console.error('Error creating POST');
        }
      }
    }
  }

  private async _retrievePost(id: string) {
    try {
      const post = await this._dataService.getPostById(id);
      this.postForm.patchValue(post);
    } catch (error) {
      console.error('Error retrieving post');
    }
  }
}
