// Angular Core
import { Component, OnDestroy, OnInit } from '@angular/core';

// Forms
import { FormControl } from '@angular/forms';

// Routing
import { Routing } from 'src/app/app-routing.module';
import { Router } from '@angular/router';

// RxJs
import { Subscription } from 'rxjs';

// Models
import { Post } from 'src/app/models/post.model';

// Services
import { DataService } from '../../../services/data/data.service';

@Component({
  selector: 'app-read',
  templateUrl: './read.component.html',
  styleUrls: ['./read.component.scss'],
})
export class ReadComponent implements OnInit, OnDestroy {
  public postArray: Post[];
  public postArraySize: number;
  public page: number;
  public pageSize: number;
  public postFilter: FormControl;

  private _fullPostArray: Post[];
  private _subscriptions: Subscription[];

  constructor(private _dataService: DataService, private _router: Router) {
    this.postArray = [];
    this._fullPostArray = [];
    this._subscriptions = [];
    this.postArraySize = 1;
    this.page = 1;
    this.pageSize = 10;
    this.postFilter = new FormControl('');
    this._subscriptions.push(
      this.postFilter.valueChanges.subscribe(async (value) => {
        if (Number.isInteger(parseInt(value))) {
          this.postArray = await this._searchPost(value);
          this.postArraySize = this.postArray.length;
          this.page = 1;
        }
      })
    );
  }

  ngOnInit(): void {
    this._fetchData();
  }

  ngOnDestroy(): void {
    this._subscriptions.forEach((suscription) => suscription.unsubscribe());
  }

  /**
   * Redirect user to edit page
   */
  redirectEdit() {
    this._router.navigateByUrl(Routing.add);
  }

  /**
   * Fetch posts data
   */
  private async _fetchData() {
    this._fullPostArray = await this._dataService.getPosts();
    this.postArraySize = this._fullPostArray.length;
    this.refreshPosts();
  }

  /**
   * Method that gets the piece of data needed for pagination
   */
  refreshPosts(): void {
    this.postArray = this._fullPostArray.slice(
      (this.page - 1) * this.pageSize,
      (this.page - 1) * this.pageSize + this.pageSize
    );
  }

  /**
   *
   * @param id User ID
   * @returns Posts belonging that user id
   */
  _searchPost(id: any): Promise<Post[]> {
    return this._dataService.filterPosts(id);
  }
}
