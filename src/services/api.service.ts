import {
  HttpClient,
  HttpErrorResponse,
  HttpParams,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class ApiService {
  constructor(private _http: HttpClient) {}

  /**
   * Common handler for get requests to API
   * @param url
   * @param params (optional)
   * @returns {Promise<any>}
   */
  get(url: string, params?: object): Promise<any> {
    return this._request('get', url, null, params);
  }

  /**
   * Common handler for post requests to API
   * @param url
   * @param body
   * @param params (optional)
   * @returns {Promise<any>}
   */
  post(url: string, body: object, params?: string): Promise<any> {
    return this._request('post', url, body, params);
  }

  /**
   * Common handler for put requests to API
   * @param url
   * @param body
   * @param params (optional)
   * @returns {Promise<any>}
   */
  put(url: string, body: object, params?: string): Promise<any> {
    return this._request('put', url, body, params);
  }

  private _request(method, url, body, params): Promise<any> {
    let httpParams = new HttpParams();
    if (params) {
      Object.keys(params).forEach((key) => {
        httpParams = httpParams.set(key, params[key]);
      });
    }

    const opts = {
      params: httpParams,
    };

    let response: Observable<any>;
    if (method === 'put' || method === 'post' || method === 'patch') {
      response = this._http[method](environment.API_URL + url, body, opts);
    } else {
      response = this._http[method](environment.API_URL + url, opts);
    }

    return new Promise((resolve, reject) => {
      response.subscribe(
        (res) => {
          if (res) {
            resolve(res);
          } else {
            console.log('Error: API Service');
            reject(res);
          }
        },
        (err: HttpErrorResponse) => {
          // TODO: Handle error in combination with error interceptor
          console.log('Error: API Service - ' + JSON.stringify(err.error));
          reject(err);
        }
      );
    });
  }
}
