import { Injectable } from '@angular/core';
import { Post } from 'src/app/models/post.model';
import { ApiService } from '../api.service';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  constructor(private _apiService: ApiService) {}

  getPosts(): Promise<Post[]> {
    return this._apiService.get('posts');
  }

  filterPosts(id: string): Promise<Post[]> {
    return this._apiService.get('posts', { userId: id });
  }

  getPostById(id: string): Promise<Post> {
    return this._apiService.get(`posts/${id}`);
  }

  updatePost(post: Post): Promise<Post> {
    return this._apiService.put(`posts/${post.id}`, { body: post });
  }

  createPost(post: Post): Promise<Post> {
    return this._apiService.post('posts', { body: post });
  }
}
