# IzertisTest

Prueba técnica realizada para Izertis usando [Angular CLI](https://github.com/angular/angular-cli) version 12.2.6.

## Servidor de desarrollo

Para lanzar la aplicación, ejecutar `npm run start` y navegar a la dirección `http://localhost:4200/`.

## Ejecutar los tests unitarios

Ejecutar `npm run test` para lanzar los test unitarios via [Jest](https://jestjs.io/es-ES/).
